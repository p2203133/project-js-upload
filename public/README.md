# projet-js

## Description
Le projet `projet-js` est un site web interactif dédié à la gestion et la présentation de photos. Il offre une interface utilisateur permettant de créer un pêle-mêle de photos de manière intuitive et engageante.

## Utilisation
Pour ajouter une photo au pêle-mêle :
1. Cliquez sur une zone vide.
2. Cliquez sur une photo pour l'ajouter.

## Contribution
**Enzo Chiattella**

## Licence
Ce projet est actuellement non sous licence et libre de droits.

## FAQ
1. **Comment ajouter des photos au pêle-mêle ?**
   - Cliquez sur une zone vide, puis sur une photo pour l'ajouter.
2. **Puis-je supprimer une photo du pêle-mêle ?**
   - Cette fonctionnalité n'est pas encore implémentée.
3. **Si je ne suis pas connécté à un réseau internet est ce que je peux quand meme utiliser le site web en local**
   - Oui, en cas d'erreur lors de la requète à l'api, des images de secours seront proposées.

## Contact
Pour toute question ou suggestion, veuillez contacter Enzo Chiattella à l'adresse suivante : enzo.chiattella@gmail.com
