/**
 * Initialise le poster avec des images de la nature depuis l'API Pexels.
 *
 * @param {Element} aside - L'élément <aside> où les images seront ajoutées.
 * @param {Element} poster - L'élément <div> du poster.
 * @param {Array} listeZone - Un tableau pour stocker les zones de dépôt.
 * @param {Array} listeImg - Un tableau pour stocker les images choissables.
 * @returns {Promise<void>} - Une promesse qui est résolue lorsque l'initialisation est terminée.
 */
async function initPoster(aside, poster, listeZone, listeImg) {
    // Clé d'API pour accéder à Pexels
    var apiKey = "utk3209w4rEGrBoSChAqYB43mIDuok2cmLnzgkHHTZnzxmNKfBUzgTfp";

    // Génère un numéro de page aléatoire pour obtenir différentes images à chaque initialisation
    var page = Math.random() * 50;

    // URL de l'API Pexels pour récupérer des images de la nature
    const apiUrl = `https://api.pexels.com/v1/search?query=nature&per_page=5&page=${page}`;

    // Options de la requête Fetch
    const options = {
        method: 'GET',
        headers: {
            'Authorization': apiKey
        }
    };

    try {
        // Effectue une requête Fetch pour obtenir les données JSON des images
        const response = await fetch(apiUrl, options);
        const data = await response.json();

        if (data.photos.length > 0) {
            // Parcours les données des photos et les ajoute à l'élément <aside>
            data.photos.forEach((photo, index) => {
                let img = document.createElement('img');
                img.src = photo.src.original;
                img.className = "img";
                listeImg.push(img);
                aside.appendChild(img);
            });
        } else {
            /*Ajout d'image de base si jamais le nombre de photo renvoyer par l'api est 0 */
            for (let index = 1; index < 6; index++) {
                let img = document.createElement('img');
                img.src = "img/img" + index + ".jpg";
                img.className = "img";
                listeImg.push(img);
                aside.appendChild(img)
            }
        }
    } catch (error) {
        /*Ajout d'image de base en cas d'erreur lors de la requete */
        for (let index = 1; index < 6; index++) {
            let img = document.createElement('img');
            img.src = "img/img" + index + ".jpg";
            img.className = "img";
            listeImg.push(img);
            aside.appendChild(img)
        }
    }

    // Crée quatre zones de dépôt dans l'élément <div> du poster
    for (let i = 1; i < 5; i++) {
        let zone = document.createElement("div");
        zone.className = "depot";
        listeZone.push(zone);
        poster.appendChild(zone);
    }
}

/**
 * Configure les écouteurs d'événements pour les zones de dépôt et les images.
 *
 * @param {Array} listeZone - Un tableau contenant les zones de dépôt.
 * @param {Array} listeImg - Un tableau contenant les images récupérées.
 * @param {Element} poster - L'élément <div> du poster.
 */
function listenerAndSeleted(listeZone, listeImg, poster) {
    var selectedZone = null;
    var isimgClicked = false;

    // Ajoute un écouteur d'événement "click" à chaque zone de dépôt
    listeZone.forEach(zoneImg => {
        zoneImg.addEventListener("click", function () {
            isimgClicked = true;
            zoneImg.className = "selected depot";
            if (selectedZone != null) {
                selectedZone.className = "depot";
            }
            selectedZone = zoneImg;
        });
    });

    // Ajoute un écouteur d'événement "click" à chaque image
    listeImg.forEach(img => {
        img.addEventListener("click", function () {
            if (selectedZone != null) {
                if (selectedZone.children.length != 0) {
                    selectedZone.removeChild(selectedZone.firstChild);
                }
                let newImg = document.createElement('img');
                newImg.src = img.src;
                newImg.id = "imgposter";
                selectedZone.appendChild(newImg);
            }
        });
    });

    // Ajoute un écouteur d'événement "click" à l'élément du poster
    poster.addEventListener("click", function () {
        if (!isimgClicked) {
            selectedZone.className = "depot";
            selectedZone = null;
        }
        isimgClicked = false;
    });
}

/**
 * Fonction principale qui initialise le poster et configure les écouteurs d'événements.
 * Appelée au chargement de la page.
 */
async function main() {
    var aside = document.querySelector('aside');
    var poster = document.getElementById("poster");
    var listeZone = [];
    var listeImg = [];
    await initPoster(aside, poster, listeZone, listeImg);
    listenerAndSeleted(listeZone, listeImg, poster);
}

// Appelle la fonction principale lors du chargement de la page
main();
